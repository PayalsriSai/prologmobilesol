﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrologMobileSolution.Model
{
    public class User
    {
        public int id { get; set; }
        public int organizationId { get; set; }
        public DateTime createdAt { get; set; }
        public string name { get; set; }
        public string email { get; set; }

    }
}
