﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrologMobileSolution.Model
{
    public class Organization
    {
        public int id { get; set; }
        public DateTime createdAt { get; set; }
        public string name { get; set; }
    }
}
