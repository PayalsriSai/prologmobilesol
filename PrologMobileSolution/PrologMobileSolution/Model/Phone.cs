﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrologMobileSolution.Model
{
    public class Phone
    {
        public int id { get; set; }
        public int userId { get; set; }
        public DateTime createdAt { get; set; }
        public long IMEI { get; set; }
        public bool Blacklist { get; set; }

    }
}
