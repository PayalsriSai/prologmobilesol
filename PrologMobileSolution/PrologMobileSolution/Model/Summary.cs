﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrologMobileSolution.Model
{
    public class Summary
    {
        public int id { get; set; }
        public string name { get; set; }
        public int blacklistTotal { get; set; }
        public int totalCount { get; set; }
        public List<Users> users { get; set; }        
    }

    public class Users
    { 
        public int id { get; set; }
        public string email { get; set; }        
        public int phoneCount { get; set; }

    }
}
