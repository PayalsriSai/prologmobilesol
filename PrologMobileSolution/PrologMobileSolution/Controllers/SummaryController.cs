﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PrologMobileSolution.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860


namespace PrologMobileSolution.Controllers
{
    //To run the project do F5 and you will get url https://localhost:44364/summary try to run this url into postman or fiddler you will get result
    [Route("[controller]")]
    [ApiController]
    public class SummaryController : ControllerBase
    {
        public static List<Organization> organizationList = new List<Organization>();
        public static List<User> userList = new List<User>();
        public static List<Phone> phoneList = new List<Phone>();
        public SummaryController()
        {
            DoStuff();
        }

        public void DoStuff()
        {
            RunAsync().Wait();
        }

        public static async Task RunAsync()
        {
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync("https://5f0ddbee704cdf0016eaea16.mockapi.io/organizations"))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    organizationList = JsonConvert.DeserializeObject<List<Organization>>(apiResponse);
                }

                using (var response = await httpClient.GetAsync("https://5f0ddbee704cdf0016eaea16.mockapi.io/organizations/1/users"))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    userList = JsonConvert.DeserializeObject<List<User>>(apiResponse);
                }

                using (var response = await httpClient.GetAsync("https://5f0ddbee704cdf0016eaea16.mockapi.io/organizations/1/users/1/phones"))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    phoneList = JsonConvert.DeserializeObject<List<Phone>>(apiResponse);
                }
            }
        }


            // GET: /<SummaryController>
            [HttpGet]
        public IEnumerable<Summary> Get()
        {
            List<Summary> summary = new List<Summary>();

            foreach (var item in organizationList)
            {
                var users = userList.Where(u => u.organizationId == item.id).Select(r => new Users() { id = r.id,
                    email = r.email,
                    phoneCount = phoneList.Where(p => p.userId == r.id).ToList().Count
                }).ToList();

                summary.Add(new Summary()
                {
                    id = item.id,
                    name = item.name,
                    totalCount = users.Where(u => u.phoneCount > 0).Sum(r => r.phoneCount),
                    blacklistTotal = phoneList.Where(p => p.Blacklist == true).ToList().Count,
                    users = users
                }) ;
               
            }

            return summary;
        }

    }
}
